package main.java;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class EncodingChecker {

	private static final String CLASS_NAME = EncodingChecker.class.getSimpleName();
	private final Logger logger;
	private final String[] ignoredFieldKeys;
	private final Map<String, String> mapToCheck;

	public EncodingChecker(final Logger logger, final String[] ignoredFieldKeys, final Map<String, String> mapToCheck){
		this.logger = logger;
		this.ignoredFieldKeys = ignoredFieldKeys;
		this.mapToCheck = mapToCheck;
	}

	/**
	 * Service that checks the given data map encoding and returns a new map with fixed values.
	 *
	 * @return a new hashmap with fixed values
	 */
	public HashMap<String,String> verifyAndFixEncoding() {

		logger.info("[" + CLASS_NAME + "] Begin encoding check...");

		final StringBuilder inputValues = new StringBuilder();
		for (final String key : mapToCheck.keySet()) {
			if (!Arrays.asList(ignoredFieldKeys).contains(key)) {
				inputValues.append(mapToCheck.get(key));
			}
		}

		final HashMap<String, String> fixedInputMap = (HashMap<String, String>) mapToCheck;

		if (isASCIIOrUtf8BrokenEncoding(inputValues.toString())) {
			logger.info("[" + CLASS_NAME + "] Bad encoding detected : UTF-8 broken into ISO-8859-1 or ASCII characters only");
			decodeData(fixedInputMap);
		} else {
			logger.info("[" + CLASS_NAME + "] No encoding error found");
		}

		logger.info("[" + CLASS_NAME + "] End encoding check.");

		return fixedInputMap;
	}

	/**
	 * Decode the given input data map with the detected charset to fix possible encoding errors and replace values
	 *
	 * @param inputMap the data map to decode
	 */
	private void decodeData(final Map<String, String> inputMap) {

		for (final String key : inputMap.keySet()) {

			final String inputValue = inputMap.get(key);

			if (Arrays.asList(ignoredFieldKeys).contains(key) || inputValue == null || inputValue.isEmpty()) {
				continue;
			}

			final String decodedValue = new String(inputValue.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
			if(!inputValue.equals(decodedValue)) {
				logger.info("[" + CLASS_NAME + "]WRONG_ENCODING_FIXED;field=[" + key + "];oldValue=[" + inputValue + "];newValue=[" + decodedValue + "];");
				inputMap.put(key, decodedValue);
			}
		}
	}

	/**
	 * Checks the given String by extracting its ISO bytes. If all of them are in the UTF-8 field of characters, it can have two meanings :
	 * - The input String was UTF-8 encoded and badly re-encoded in ISO
	 * - Or the input String contained only ASCII chars (which has no influence on encoding conversion between ISO-8859-1 and UTF-8)
	 *
	 * @param encodedData the String to check
	 * @return true if the input string has only ISO bytes that are UTF-8
	 */
	private boolean isASCIIOrUtf8BrokenEncoding(final String encodedData) {

		logger.fine("[" + CLASS_NAME + "] Begin utf-8 broken characters detection...");

		final byte[] encodedDataBytes = encodedData.getBytes(StandardCharsets.ISO_8859_1);

		int offset = 0;
		while (offset < encodedDataBytes.length) {
			if (!isUTF8(encodedDataBytes, offset)) {
				logger.fine("[" + CLASS_NAME + "] Byte not UTF-8 compliant detected, data not UTF-8 broken");
				return false;
			}
			offset += isSingleCharUtf8(encodedDataBytes, offset) ? 1:2;
		}

			logger.fine("[" + CLASS_NAME + "] All ISO bytes are UTF-8 compliant, broken encoding detected or ASCII only characters");

		return true;
	}

	/**
	 * Check if the byte in the given array at the specified offset is inside the utf-8 char byte table, either in the single byte char table or in the double.
	 *
	 * @param encodedDataByte the full bytes array
	 * @param offset the offset to check the byte(s)
	 * @return true if the byte or the next two bytes are in the utf-8 table
	 */
	private boolean isUTF8(final byte[] encodedDataByte, final int offset) {
		return isSingleCharUtf8(encodedDataByte, offset) || isDoubleCharUtf8(encodedDataByte, offset);
	}

	/**
	 * Check if the byte in the given array at the specified offset is between the borders of possible single byte utf-8 chars.
	 *
	 * @param encodedDataByte the full bytes array
	 * @param offset the offset to check the byte
	 * @return true if the byte is a possible single byte utf-8 char
	 */
	private boolean isSingleCharUtf8(final byte[] encodedDataByte, final int offset) {
		final byte b = encodedDataByte[offset];
		return (byte) 0x20 <= b && b < (byte) 0x7f;
	}

	/**
	 * Check if the byte in the given array at the specified offset is an utf8 double byte marker (0xc2 or 0xc3)
	 * and if the next byte is between the respective utf-8 bytes borders.
	 *
	 * @param encodedDataByte the full bytes array
	 * @param offset the offset to check the pair of bytes
	 * @return true if there is a specific utf8 pair of bytes
	 */
	private boolean isDoubleCharUtf8(final byte[] encodedDataByte, final int offset) {
		final byte firstByte = encodedDataByte[offset];
		if (offset + 1 >= encodedDataByte.length) {
			return false;
		}
		final byte secondByte;
		if (firstByte == (byte) 0xc2) {
			secondByte = encodedDataByte[offset + 1];
			return (byte) 0xa0 <= secondByte && secondByte <= (byte) 0xbf;
		} else if (firstByte == (byte) 0xc3){
			secondByte = encodedDataByte[offset + 1];
			return (byte) 0x80 <= secondByte && secondByte <= (byte) 0xbf;
		}
		return false;
	}

}
