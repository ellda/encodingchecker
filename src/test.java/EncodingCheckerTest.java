import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import main.java.EncodingChecker;

public class EncodingCheckerTest {

	private final Logger testConsoleLogger = Logger.getLogger("ConsoleAppender");
	private Map<String, String> testMap;
	private final String MAP_TEST_KEY = "testKey";
	private final String MAP_IGNORED_KEY = "ignoredKey";
	private final String[] MAP_IGNORED_KEY_LIST = new String[]{MAP_IGNORED_KEY};
	private final String BASE_STR_TEST = "CADDIE!2£254¤0MA_BOUtiQUE¤téàsÂ©t¤££Â§Â§£";
	private final String BASE_STR_TEST_ASCII = "abcdefg1238AZ";
	private final String IGNORE_STR_TEST = "ignoredValue£2¤0iE¤téàÂ©t¤£§Â§£";

	private main.java.EncodingChecker encodingChecker;


	@Before
	public void setUp() {
		testMap = new HashMap<>();
		testMap.put(MAP_IGNORED_KEY, IGNORE_STR_TEST);
	}

	private void initEncodingChecker(){
		encodingChecker = new EncodingChecker(testConsoleLogger, MAP_IGNORED_KEY_LIST, testMap);
	}

	@Test
	public void testVerifyAndFixEncoding_iso_correctEncoding() {
		parametrisedTestVerifyAndFixEncoding(BASE_STR_TEST, ISO_8859_1, ISO_8859_1);
	}

	@Test
	public void testVerifyAndFixEncoding_utf8_correctEncoding() {
		parametrisedTestVerifyAndFixEncoding(BASE_STR_TEST, UTF_8, UTF_8);
	}

	@Test
	public void testVerifyAndFixEncoding_utf8_wrongEncoding() {
		parametrisedTestVerifyAndFixEncoding(BASE_STR_TEST, UTF_8, ISO_8859_1);
	}

	@Test
	public void testVerifyAndFixEncoding_iso_correctEncoding_asciiCharsOnly() {
		parametrisedTestVerifyAndFixEncoding(BASE_STR_TEST_ASCII, ISO_8859_1, ISO_8859_1);
	}

	@Test
	public void testVerifyAndFixEncoding_utf8_correctEncoding_asciiCharsOnly() {
		parametrisedTestVerifyAndFixEncoding(BASE_STR_TEST_ASCII, UTF_8, UTF_8);
	}

	@Test
	public void testVerifyAndFixEncoding_utf8_wrongEncoding_asciiCharsOnly() {
		parametrisedTestVerifyAndFixEncoding(BASE_STR_TEST_ASCII, UTF_8, ISO_8859_1);
	}

	@Test
	public void testVerifyAndFixEncoding_nullValue() {
		testMap.put(MAP_TEST_KEY, null);
		initEncodingChecker();

		final HashMap<String, String> decodedData = encodingChecker.verifyAndFixEncoding();
		Assert.assertNull(decodedData.get(MAP_TEST_KEY));
	}

	@Test
	public void testVerifyAndFixEncoding_emptyValue() {
		testMap.put(MAP_TEST_KEY, "");
		initEncodingChecker();

		final HashMap<String, String> decodedData = encodingChecker.verifyAndFixEncoding();
		Assert.assertEquals("", decodedData.get(MAP_TEST_KEY));
	}

	/**
	 * Test method used to simplify other tests with multiple parameters combination among string used and charsets.
	 *
	 * @param stringToUse the String to convert and test
	 * @param originCharset the original charset of the string to use
	 * @param forcedCharset the forced charset to re-encode the string to use
	 */
	private void parametrisedTestVerifyAndFixEncoding(final String stringToUse, final Charset originCharset, final Charset forcedCharset) {
		testMap.put(MAP_TEST_KEY, new String(stringToUse.getBytes(originCharset), forcedCharset));
		initEncodingChecker();

		final HashMap<String, String> decodedData = encodingChecker.verifyAndFixEncoding();

		Assert.assertEquals(stringToUse, decodedData.get(MAP_TEST_KEY));
		Assert.assertEquals(IGNORE_STR_TEST, decodedData.get(MAP_IGNORED_KEY));
	}

	@Test
	public void testIsASCIIOrUtf8BrokenEncoding_iso_correctEncoding() throws Exception {
		Assert.assertFalse(parametrisedTestIsASCIIorUTF8BrokenEncoding(BASE_STR_TEST, ISO_8859_1, ISO_8859_1));
	}

	@Test
	public void testIsASCIIOrUtf8BrokenEncoding_utf8_correctEncoding() throws Exception {
		Assert.assertFalse(parametrisedTestIsASCIIorUTF8BrokenEncoding(BASE_STR_TEST, UTF_8, UTF_8));
	}

	@Test
	public void testIsASCIIOrUtf8BrokenEncoding_utf8_wrongEncoding() throws Exception {
		Assert.assertTrue(parametrisedTestIsASCIIorUTF8BrokenEncoding(BASE_STR_TEST, UTF_8, ISO_8859_1));
	}

	@Test
	public void testIsASCIIOrUtf8BrokenEncoding_iso_correctEncoding_asciiCharsOnly() throws Exception {
		Assert.assertTrue(parametrisedTestIsASCIIorUTF8BrokenEncoding(BASE_STR_TEST_ASCII, ISO_8859_1, ISO_8859_1));
	}

	@Test
	public void testIsASCIIOrUtf8BrokenEncoding_utf8_correctEncoding_asciiCharsOnly() throws Exception {
		Assert.assertTrue(parametrisedTestIsASCIIorUTF8BrokenEncoding(BASE_STR_TEST_ASCII, UTF_8, UTF_8));
	}

	@Test
	public void testIsASCIIOrUtf8BrokenEncoding_utf8_wrongEncoding_asciiCharsOnly() throws Exception {
		Assert.assertTrue(parametrisedTestIsASCIIorUTF8BrokenEncoding(BASE_STR_TEST_ASCII, UTF_8, ISO_8859_1));
	}

	/**
	 * Test method used to simplify other tests for isASCIIorUTF8BrokenEncoding private method with multiple parameters combination among string used and charsets.
	 *
	 * @param stringToUse the String to convert and test
	 * @param originCharset the original charset of the string to use
	 * @param forcedCharset the forced charset to re-encode the string to use
	 * @return the boolean returned by the tested method
	 */
	private boolean parametrisedTestIsASCIIorUTF8BrokenEncoding(final String stringToUse, final Charset originCharset, final Charset forcedCharset) throws Exception {
		final Method isASCIIOrUtf8BrokenEncodingMethod = EncodingChecker.class.getDeclaredMethod("isASCIIOrUtf8BrokenEncoding", String.class);
		isASCIIOrUtf8BrokenEncodingMethod.setAccessible(true);

		initEncodingChecker();

		final String reEncodedString = new String(stringToUse.getBytes(originCharset), forcedCharset);
		return (boolean) isASCIIOrUtf8BrokenEncodingMethod.invoke(encodingChecker, reEncodedString);
	}
}
